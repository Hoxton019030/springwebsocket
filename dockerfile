FROM openjdk

WORKDIR /app
COPY target/springboot-websocket-0.0.1-SNAPSHOT.jar /app
EXPOSE 8080
CMD ["java","-jar","/app/springboot-websocket-0.0.1-SNAPSHOT.jar"]