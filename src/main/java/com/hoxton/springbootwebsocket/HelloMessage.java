package com.hoxton.springbootwebsocket;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Hoxton on 2023/2/17
 * @since 1.2.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HelloMessage {

    private String name;
}
